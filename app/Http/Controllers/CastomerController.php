<?php

namespace App\Http\Controllers;

use App\Castomer;
use App\Account;
use App\Deposit;
use Illuminate\Http\Request;

class CastomerController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $castomers = Castomer::paginate(15);

        return view('castomer.index',compact('castomers'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('castomer.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $castomer = new Castomer();
        $data = $this->validate($request, [
            'name' => 'required',
            'soname' => 'required',
            'sex' => 'required',
            'birthday' => 'required',
            'ind_number' => 'required',
        ]);

        $castomer->saveCastomer($data);
        return redirect('/castomer')->with('success', 'New castomer has been created!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $castomer = Castomer::find($id);

        return view('castomer.edit', compact('castomer', 'id'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $castomer = new Castomer();
        $data = $this->validate($request, [
            'name' => 'required',
            'soname' => 'required',
            'sex' => 'required',
            'birthday' => 'required',
            'ind_number' => 'required',
        ]);
        $data['id'] = $id;
        $castomer->updateCastomer($data);

        return redirect('/castomer')->with('success', 'Castomer has been updated!!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function deposit($id)
    {
        $castomer = Castomer::find($id);
        $deposits = $castomer->deposits;

        return view('castomer.deposit',[
            'castomer' => $castomer,
            'deposits' => $deposits,
        ]);
    }

    /**
     * @param $castomer_id
     * @param $deposit_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function transaction($castomer_id, $deposit_id)
    {
        $account = Account::where('deposit_id', '=', $deposit_id)->get();

        return view('castomer.account',[
            'account' => $account,
        ]);
    }
}
