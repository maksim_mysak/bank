<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CalculationRepository;

class DepositController extends Controller 
{
    /**
     * @var CalculationRepository
     */
    protected $calculation;


    public function __construct(CalculationRepository $calculation)
    {
        $this->calculation = $calculation;
    }

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        return view('report.group', [

            'groupe1' => $this->calculation->reportMiddelSumDeposit(18, 25),
            'groupe2' => $this->calculation->reportMiddelSumDeposit(25, 50),
            'groupe3' => $this->calculation->reportMiddelSumDeposit(0, 50),

        ]);

    }

    /**
     *
     */
    public function calculation()
    {
      $this->calculation->start();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function report()
    {
        $report = $this->calculation->report();

        return view('report.m', ['report' => $report]);
    }
}

?>