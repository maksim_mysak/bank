<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    const TYPE_COMMISION = 'commision';
    const TYPE_EARNING = 'earning';
    /**
     * @var string
     */
    protected $table = 'accounts';

    /**
     * @var bool
     */
    public $timestamps = true;

}