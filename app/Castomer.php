<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Castomer
 * @package App
 */
class Castomer extends Model
{
    /**
     * @var string
     */
    protected $table = 'castomers';

    /**
     * @param $data
     * @return int
     */
    public function saveCastomer($data): int
    {
        $this->name = $data['name'];
        $this->soname = $data['soname'];
        $this->sex = $data['sex'];
        $this->birthday = $data['birthday'];
        $this->ind_number = $data['ind_number'];
        $this->save();

        return 1;
    }

    /**
     * @param $data
     * @return int
     */
    public function updateCastomer($data): int
    {
        $castomer = $this->find($data['id']);
        $castomer->name = $data['name'];
        $castomer->soname = $data['soname'];
        $castomer->sex = $data['sex'];
        $castomer->birthday = $data['birthday'];
        $castomer->ind_number = $data['ind_number'];
        $castomer->save();

        return 1;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deposits()
    {
        return $this->HasMany(\App\Deposit::class, 'castomer_id', 'id');
    }
}
