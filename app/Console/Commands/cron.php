<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\CalculationRepository;

class cron extends Command
{
    /**
     * @var CalculationRepository
     */
    protected $calculation;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculation:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculation sum from deposit castomer';

    /**
     * cron constructor.
     * @param CalculationRepository $calculation
     */
    public function __construct(CalculationRepository $calculation)
    {
        $this->calculation = $calculation;
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Calculation successful');
        $this->calculation->start();
    }
}
