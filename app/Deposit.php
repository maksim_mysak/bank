<?php

namespace App;
use App\Castomer;
use Illuminate\Database\Eloquent\Model;

class Deposit extends Model 
{

    protected $table = 'deposits';
    public $timestamps = true;

    /**
     * @return BelongsTo
     */
    public function castomer()
    {
        return $this->belongsTo(\App\Castomer::class, 'castomer_id');
    }
}