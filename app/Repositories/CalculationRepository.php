<?php

namespace App\Repositories;

use App\Account;
use App\Deposit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Repositories\Contacts\CalculationRepositoryInterface as CalculationRepositoryInterface;

/**
 * Class CalculationRepository
 * @package App\Repositories
 */
class CalculationRepository implements CalculationRepositoryInterface
{
    /**
     * @var Deposit
     */
    protected $deposit;

    public function __construct(Deposit $deposit)
    {
        $this->deposit = $deposit;
    }

    public function all()
    {
        // TODO: Implement all() method.
    }

    public function start()
    {
        $deposit = $this->deposit->where('active','=', true)->get();

        foreach ($deposit as $item) {

            $now = Carbon::now();
            $data_created = Carbon::parse($item->created_at);

            if($now->day == $data_created->day) {
                DB::beginTransaction();
                try{
                    $this->setEarning($item);
                }
                catch(QueryException $e){
                    DB::rollBack();
                }
                DB::commit();
            }

            if($now->day == 1) {
                DB::beginTransaction();
                try {
                    $this->setCommission($item);
                } catch (QueryException $e) {
                    DB::rollBack();
                }
                DB::commit();
            }

        }
    }


    private function commission($amount)
    {
        if ($amount >= 0 && $amount <= 1000)  {
            $c = $amount*5/100;
            if ($c < 50){
                return 50;
            }
            return $c;

        } elseif ($amount >= 1000 && $amount <= 10000) {

            return $amount*6/100;

        } elseif($amount > 10000) {
            $c = $amount*7/100;
            if ($c > 5000){

                return 5000;
            }

            return $c;
        }
    }

    /**
     * @param $id
     * @return int
     */
    private function calcDepositCommission($id)
    {
        $deposit = $this->deposit->find($id);
        $data_created = $deposit->created_at;
        $amount = $this->getAmount($deposit);
        $now = Carbon::now();
        $data_created = Carbon::parse($data_created);
        $days_btw = $data_created->diffInDays($now);

        $commision = $this->commission($amount);
        if ($days_btw < 30) {
            $commision = (1/$data_created->day) * $commision;
        }

        return $commision;
    }

    /**
     * @param $deposit
     */
    private function setCommission($deposit)
    {
        $account = new Account();
        $commision = $this->calcDepositCommission($deposit->id);

        $deposit_amount = $this->getAmount($deposit);

        $account->type = $account::TYPE_COMMISION;
        $account->deposit_id = $deposit->id;
        $account->amount_current = $deposit_amount - $commision;
        $account->amount = $commision;
        $account->save();

        $deposit->amount_current = $deposit_amount - $commision;
        $deposit->save();

    }

    /**
     * @param int $i – годовая процентная ставка;
     * @param int $j – количество календарных дней в периоде, по итогам которого банк производит         капитализацию начисленных процентов;
     * @param int $k – количество дней в календарном году (365 или 366);
     * @param int $p – первоначальная сумма привлеченных в депозит денежных средств;
     * @return  int Sp – сумма процентов (доходов).
     */
    private function calcDeposit(
        $i,
        $j = 30,
        $k = 365,
        $p
        ){
        $dt = Carbon::now();
        $k = ($dt->isLeapYear() ? 366 : 365);
       return $p * $i * $j / $k / 100;
    }

    /**
     * @param $deposit
     */
    private function setEarning($deposit)
    {
        //check date last calculations
        if ($this->checkDate($deposit)) {
            return false;
        }

        $amount = $this->getAmount($deposit);
        $amount_pesent = $this->calcDeposit(
            $deposit->rate,
            $j = 30,
            $k = 365,
            $amount
        );
        $amount_current = $amount_pesent + $amount;
        //**/
        $account = new Account();
        $account->type = $account::TYPE_EARNING;
        $account->deposit_id = $deposit->id;
        $account->amount_current = $amount_current;
        $account->amount = $amount_pesent;
        $account->save();

        $deposit->amount_current = $amount_current;
        $deposit->save();

        //check active deposit calculations
        $this->checkActive($deposit);

    }

    /**
     * @param $deposit
     * @return mixed
     */
    private function getAmount($deposit)
    {
        $deposit_amount = $deposit->amount;
        if ($deposit->amount_current > 0) {
            $deposit_amount = $deposit->amount_current;
        }
        return $deposit_amount;
    }

    /**
     * @param $deposit
     * @return bool
     */
    private function checkDate($deposit)
    {
        $data_created = $deposit->created_at;
        $data_created_deposit = Carbon::parse($data_created);
        $dt = Carbon::now();

        if ($data_created_deposit->day == $dt->day) {
            return false;
        }

        return true;
    }

    /**
     * @param $deposit
     */
    private function checkActive($deposit)
    {
       $sum =  Account::where([
           ['deposit_id','=', $deposit->id],
           ['type', '=', 'earning']]
       )->sum('day_time');

        if ($deposit->day_period < $sum){
            $deposit->active = false;
            $deposit->save();
        }
    }

    /**
     * @return mixed
     */
    public function report()
    {
        return DB::table('accounts')
            ->select(DB::raw('MONTH(created_at)  as month'))
            ->addSelect(DB::raw('SUM(amount) as total_sum'))
            ->addSelect(DB::raw('SUM(IF(type = \'earning\', amount, 0)) AS earning'))
            ->addSelect(DB::raw('SUM(IF(type = \'commision\', amount, 0)) AS commision'))
            ->groupBy('month')
            ->get();
    }

    /**
     * @param int $from
     * @param int $to
     * @return mixed
     */
    public function reportMiddelSumDeposit(int $from, int $to)
    {
        $havingRaw = 'age BETWEEN ' . $from . ' AND ' . $to;
        if ($from == 0) {
            $havingRaw = 'age > ' . $to;
        }

        return DB::table('deposits')
            ->addSelect(DB::raw('amount'))
            ->addSelect(DB::raw('SUM(amount) as sum_deposit'))
            ->addSelect(DB::raw('castomer_id as castomer_id'))
            ->addSelect(DB::raw('COUNT(castomer_id) as deposit_count'))
            ->addSelect(DB::raw('((YEAR(CURRENT_DATE) - YEAR(birthday)) - (DATE_FORMAT(CURRENT_DATE, \'%m%d\') < DATE_FORMAT(birthday, \'%m%d\'))) AS age'))
            ->leftJoin('castomers', function($join) {
                $join->on('deposits.castomer_id', '=', 'castomers.id');
            })
            ->groupBy('castomer_id')
            ->havingRaw($havingRaw)
            ->get()->avg('sum_deposit');
    }
}