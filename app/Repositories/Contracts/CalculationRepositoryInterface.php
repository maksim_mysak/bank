<?php

namespace App\Repositories\Contacts;

use Illuminate\Http\Request;

/**
 * Interface CalculationRepositoryInterface
 * @package App\Repositories\Contacts
 */
interface CalculationRepositoryInterface
{
    /**
     * @return mixed
     */
    public function all();

}