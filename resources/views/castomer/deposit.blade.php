@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>deposit castomers : {{$castomer->name}}</h1>

        <table class="table table-striped">
            <thead>
            <tr>
                <td>Date created</td>
                <td>Rate %</td>
                <td>Amount $</td>
                <td>Accrued $</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($deposits as $deposit)
                <tr>
                    <td>
                        {{ \Carbon\Carbon::parse($deposit->created_at)->format('Y-m-d') }}
                        @if($deposit->active)
                            <span class="badge badge-success badge-pill">active</span>
                        @else
                            <span class="badge badge-danger badge-pill">close</span>
                        @endif
                    </td>
                    <td>{{ $deposit->rate}} %</td>
                    <td>{{ $deposit->amount}} $</td>
                    <td>{{ $deposit->amount_current}} $</td>
                    <td>
                        <a href="/castomer/{{$castomer->id}}/{{ $deposit->id}}/transactions"
                           class="btn btn-primary"
                           role="button">transactions</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    <div>
@endsection