@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>castomers <a href="{{url('/castomer/new')}}" class="btn btn-primary">create</a></h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <td>Идентификационный номер</td>
                <td>Имя, Фамилия</td>
                <td>Пол</td>
                <td>Дата Рождения</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($castomers as $castomer)
                <tr>
                    <td>{{$castomer->ind_number}}</td>
                    <td>{{$castomer->name}} {{$castomer->soname}}</td>
                    <td>{{$castomer->sex}}</td>
                    <td>{{ \Carbon\Carbon::parse($castomer->birthday)->format('Y-m-d') }}</td>
                    <td>
                        <a class="btn btn-primary"
                           href="{{url('/castomer/' . $castomer->id. '/deposit')}}"
                           role="button">deposit</a>

                        <a class="btn btn-primary"
                           href="{{url('/castomer/' . $castomer->id. '/edit')}}"
                           role="button">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div>

        <div class="row">{{ $castomers->links() }}</div>
@endsection