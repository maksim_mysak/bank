@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>trnsactions</h1>

        <table class="table table-striped">
            <thead>
            <tr>
                <td>Date created</td>
                <td>type</td>
                <td>amount</td>
            </tr>
            </thead>
            <tbody>
            @foreach($account as $ac)
                <tr>
                    <td>{{ \Carbon\Carbon::parse($ac->created_at)->format('Y-m-d') }}</td>
                    <td>
                        @if($ac->type =='commision')
                            <span class="badge badge-danger badge-pill">-{{$ac->amount}}</span>
                        @else
                            <span class="badge badge-success badge-pill">+{{$ac->amount}}</span>
                        @endif
                    </td>
                    <td><span class="badge badge-success badge-pill">{{$ac->amount_current}}$</span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div>
@endsection