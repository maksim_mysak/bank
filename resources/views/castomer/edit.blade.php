@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>new castomer</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif

        <div class="row">
            <div class="col-md-8 order-md-1">
                <form method="post" action="{{url('/castomer/'.$id.'/update')}}">
                    <div class="form-group">
                        <input type="hidden" value="{{csrf_token()}}" name="_token" />
                        <label for="title">Идентификационный номер</label>
                        <input type="text" class="form-control" value="{{$castomer->ind_number}}" name="ind_number" required/>
                    </div>

                    <div class="form-group">
                        <label for="title">Имя</label>
                        <input type="text" class="form-control" value="{{$castomer->name}}" name="name" required/>
                    </div>

                    <div class="form-group">
                        <label for="title">Фамилия</label>
                        <input type="text" class="form-control" value="{{$castomer->soname}}" name="soname" required/>
                    </div>

                    <div class="form-group">
                        <label for="title">Дата Рождения</label>
                        <input type="text" class="form-control" value="{{$castomer->birthday}}"
                               name="birthday" required/>
                    </div>

                    <div class="form-group">
                        <label for="description">Пол:</label>
                        <select name="sex" class="form-control">
                            <option @if ($castomer->sex === 'male') selected @endif value="male">male</option>
                            <option @if ($castomer->sex === 'female') selected @endif value="female">female</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection