@extends('layouts.app')

@section('content')
    <div class="container">
        <h4>Убыток или прибыль банка по месяцам. (Сумма комиссий - Сумма начисленных процентов)</h4>
        <table class="table table-striped">
            <thead>
            <tr>
                <td>Месяц</td>
                <td>Сумма комиссий</td>
                <td>Сумма начисленных процентов</td>
                <td>прибыль</td>
            </tr>
            </thead>
            <tbody>
            @foreach($report as $repo)
                <tr>
                    <td>{{$repo->month}}</td>
                    <td>{{$repo->earning}}</td>
                    <td>{{$repo->commision}}</td>
                    <td>{{$repo->commision - $repo->earning}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div>
@endsection