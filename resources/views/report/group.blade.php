@extends('layouts.app')

@section('content')
    <div class="container">
        <h4>Средняя сумма депозита (Сумма депозитов/Количество депозитов) для
            возрастных групп:</h4>
        <table class="table table-striped">
            <thead>
            <tr>
                <td>I группа - От 18 до 25 лет</td>
                <td>II группа - От 25 до 50 лет</td>
                <td>III группа - От 50 лет</td>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$groupe1}}</td>
                    <td>{{$groupe2}}</td>
                    <td>{{$groupe3}}</td>
                </tr>
            </tbody>
        </table>
        <div>
@endsection