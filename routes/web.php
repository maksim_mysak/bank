<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// CastomerController
Route::get('castomer/','CastomerController@index');
Route::get('castomer/new','CastomerController@create');
Route::get('castomer/{id}/edit','CastomerController@edit');
Route::post('castomer/{id}/update','CastomerController@update');
Route::get('castomer/{id}/deposit','CastomerController@deposit');
Route::get('castomer/{castomer_id}/{deposit_id}/transactions','CastomerController@transaction');


Route::get('deposits/','DepositController@calculation');
Route::get('report/','DepositController@report');
Route::get('report_deposit/','DepositController@index');