<?php

use Illuminate\Database\Seeder;

class DepositSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!\App\Castomer::count()) {
            $this->call(CastomerSeeder::class);
        }
        $castomers = \App\Castomer::all();

        factory(\App\Deposit::class, 30)->make()->each(function (\App\Deposit $m) use ($castomers) {
            $m->castomer()->associate($castomers->random());
            $m->save();
        });
    }
}
