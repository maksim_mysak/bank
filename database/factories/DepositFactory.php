<?php

use Faker\Generator as Faker;


$factory->define(App\Deposit::class, function (Faker $faker) {
    return [
        'rate' => $faker->randomElement($array = array ('10','15','22','5')),
        'description' => 'some text',
        'amount_current' => 0,
        'amount' => $faker->randomElement($array = array (25,50,1000,10000, 89234, 7777)),
    ];
});
