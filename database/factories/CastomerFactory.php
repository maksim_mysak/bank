<?php

use Faker\Generator as Faker;


$factory->define(App\Castomer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'soname' => $faker->lastName,
        'sex' => $faker->randomElement($array = array ('male','female')),
        'birthday' => $faker->dateTimeBetween('-45 years', '-10 years'),
        'ind_number' => $faker->unique()->randomNumber().time(),
    ];
});