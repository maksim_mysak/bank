<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
        Schema::table('accounts', function(Blueprint $table) {
            $table->foreign('deposit_id')->references('id')->on('deposits');
        });
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
	public function down()
	{
		Schema::table('accounts', function(Blueprint $table) {
			$table->dropForeign('accounts_deposit_id_foreign');
		});
	}
}