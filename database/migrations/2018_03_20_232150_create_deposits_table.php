<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepositsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('deposits', function(Blueprint $table) {
			$table->timestamps();
			$table->increments('id');
			$table->string('description');
			$table->double('rate');
			$table->decimal('amount');
            $table->integer('day_period')->default(365);
            $table->boolean('active')->default(true);
            $table->decimal('amount_current');
            $table->integer('castomer_id')->unsigned();
            $table->foreign('castomer_id')->references('id')->on('castomers')->onDelete('cascade');
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
	public function down()
	{
		Schema::drop('deposits');
	}
}