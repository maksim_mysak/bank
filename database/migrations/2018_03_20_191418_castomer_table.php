<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CastomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('castomers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable(); //Имя
            $table->string('soname')->nullable(); //Фамилия
            $table->string('sex')->nullable(); //Пол
            $table->string('ind_number')->unique(); //Идентификационный номер
            $table->timestamp('birthday')->nullable(); //Дата Рождения
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('castomers');
    }
}
