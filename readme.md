## Installation Steps


### 1. git clone git@bitbucket.org:maksim_mysak/bank.git

### 2. Add the DB Credentials & APP_URL

Next make sure to create a new database and add your database credentials to your .env file:

```
DB_HOST=localhost
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

```
cd bank
composer install
php artisan migrate
php artisan db:seed
```
 

add cron jon 

<code>
10 3 * * * php /path-to-your-project/bank/artisan calculation:job >> /dev/null 2>&1
<code>